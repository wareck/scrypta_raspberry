#!/bin/bash
version="1.1"
release_date="13/03/2017"
mod="mod"

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root. use sudo su" 1>&2
   exit 1
fi

echo ""
echo -e "\e[97mScrypta_Mod autoinstall \e[92m"V$version"\e[0m\e[97m\e[0m"
echo -e "wareck@gmail.com $release_date"
echo ""

apt-get update
apt-get install -y lighttpd unzip wget openssl
apt-get install -y  php5-common php5-cgi php5

if [ ! -d /etc/php5/conf.d/ ] ; then
		mkdir -p /etc/php5/conf.d/
fi

apt-get install -y  php5-rrd  php-auth-sasl php-mail php-net-smtp php-net-socket rrdtool php5-json
apt-get install -y  libexpect-php5 ntpdate screen
apt-get install -y build-essential autoconf automake libtool pkg-config libcurl4-openssl-dev libudev-dev libjansson-dev libncurses5-dev

echo -e "\e[93mEnabling lighttpd:\e[0m"
lighty-enable-mod fastcgi-php
/etc/init.d/lighttpd restart

if [ ! -d /etc/lighttpd/certs ]
	then
		mkdir /etc/lighttpd/certs
fi

cd /etc/lighttpd/certs
openssl req -new -x509 -keyout lighttpd.pem -out lighttpd.pem -days 365 -nodes -subj "/C=US/ST=TEC/L=LONDON/O=DIS/CN=scripta.minereu.com" 
chmod 400 lighttpd.pem

ssl_option=`grep "ssl.pemfile" /etc/lighttpd/lighttpd.conf`
if [ "$ssl_option" == "" ];
	then
echo '$SERVER["socket"] == ":443" { ssl.engine = "enable" ssl.pemfile = "/etc/lighttpd/certs/lighttpd.pem" }' | tee -a  /etc/lighttpd/lighttpd.conf
fi

sed -i -e "s/var\/www\/html/var\/www/g" /etc/lighttpd/lighttpd.conf
cd /

/etc/init.d/lighttpd restart


#install scripta
#forked version from mox235 and updated by wareck
#to udpate a few configuration to support the G-Blade 40 chip

if [ $mod = "mod" ]
then
	cd /tmp
	git clone https://wareck@bitbucket.org/wareck/scripta_mod.git scripta_gc
	cd scripta_gc/
else
	cd /tmp
	git clone https://wareck@bitbucket.org/wareck/scripta_gc.git
	cd scripta_gc/
fi

echo -e "\e[93mPerforming backup:\e[0m"
backup_path=/opt/minereu_back$(date +"%Y%m%d%H%M%S")
mkdir $backup_path
mv -f -v /var/www/ $backup_path 2>/dev/null; true
mv -f -v /opt/scripta/ $backup_path 2>/dev/null; true
rm -fr /etc/cron.d/timeupdate

echo -e "\e[93mPerforming install Scrypta:\e[0m"

cp -fr usr etc opt var /
chown -R  www-data /var/www
chown -R  www-data /opt/scripta/
rm /opt/scripta/etc/cron.d/5min/inc
ln -s /var/www/inc /opt/scripta/etc/cron.d/5min/inc
chmod +x /opt/scripta/etc/cron.d/5min/hashrate
ln -s /opt/scripta/http/rrd /var/www/rrd

chmod -R +x /var/www/
chmod -R +x  /opt/scripta/bin/
chmod -R +x  /opt/scripta/startup/
chown root:root /var/spool/cron/crontabs/root

#generate the uniqe id for this device
#echo -n $(cat /proc/cpuinfo|grep Serial|awk '{print $3}')|md5sum|awk '{print $1}'
echo -e "\e[93mPerforming install cgminer:\e[0m"
apt-get install -y  build-essential git pkg-config libtool libcurl4-openssl-dev libncurses5-dev libudev-dev autoconf automake

if [ ! -d /opt/cgminer-lketc ]
then
cd /opt
git clone https://github.com/wareck/cgminer-lketc.git
cd /opt/cgminer-lketc
./autogen.sh
./configure --enable-scrypt --enable-gridseed --enable-zeus --enable-lketc
else
cd /opt/cgminer-lketc
git pull
./configure --enable-scrypt --enable-gridseed --enable-zeus --enable-lketc
fi
make install

chmod +x /usr/local/bin/cgminer

cd /tmp/scripta_gc/

#fix the bug that pi stuck when mining
if [ -f /boot/cmdline.txt ]; then
slub_debug_content=$(grep slub_debug< /boot/cmdline.txt)
fi
if  [ "$slub_debug_content" == "" ] && [ ! "$is_raspbian" == "" ]
	then
	#append  slub_debug=FPUZ to existing content and write back
	 echo  "$(cat /boot/cmdline.txt) slub_debug=FPUZ" > /boot/cmdline.txt
fi
echo -e "\e[93mPerforming start Scrypta:\e[0m"

/opt/scripta/startup/miner-stop.sh
/opt/scripta/startup/miner-start.sh
